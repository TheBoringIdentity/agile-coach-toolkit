package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.team.*;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RenameTeamTest {
    @Test
    public void renameTeam_changes_team_name() throws RenamedTeamDoesNotExist {
        var team = createTeamWithName("prename");
        team.setName(new TeamName("prename"));
        team.setId("x");
        TeamRepository teamRepo = mock(TeamRepository.class);
        when(teamRepo.findTeam(anyString())).thenReturn(team);

        RenameTeam bus = new RenameTeam(teamRepo, mock(DomainEventDispatcher.class));
        bus.renameTeam(team.getId(), new TeamName("aftername"));

        assertThat(team.getName()).isEqualTo(new TeamName("aftername"));
    }

    private Team createTeamWithName(String name) {
        return new TeamBuilder(new TeamName(name)).build();
    }

    @Test
    public void renaming_nonexisting_team_fails() {
        TeamRepository repository = mock(TeamRepository.class);
        when(repository.findTeam(anyString())).thenReturn(null);

        var command = new RenameTeam(repository, mock(DomainEventDispatcher.class));

        assertThrows(RenamedTeamDoesNotExist.class, () -> command.renameTeam("x", new TeamName("newName")));
    }

    @Test
    public void pending_events_are_dispatched_when_name_was_changed() throws RenamedTeamDoesNotExist {
        var team = createTeamWithName("prename");
        team.setName(new TeamName("prename"));
        team.setId("x");

        var teamRepo = mock(TeamRepository.class);
        when(teamRepo.findTeam(anyString())).thenReturn(team);

        var mockDispatcher = mock(DomainEventDispatcher.class);

        var command = new RenameTeam(teamRepo, mockDispatcher);
        command.renameTeam(team.getId(), new TeamName("aftername"));

        Mockito.verify(mockDispatcher).dispatch(any(TeamRenamedEvent.class));
    }
}
