package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class RegisterPersonTest {
    @Test
    public void registering_valid_person_creates_and_stores_record_in_repository() {
        var person = new Person();
        person.setId("p-id");
        var mockRepo = mock(PersonRepository.class);
        when(mockRepo.createPerson()).thenReturn(person);

        var command = new RegisterPerson(mockRepo);
        var result = command.register(new PersonName("Max Mustermann"));

        verify(mockRepo).createPerson();
        verify(mockRepo).savePerson(person);

        assertThat(result.getName()).isEqualTo(new PersonName("Max Mustermann"));
    }

    @Test
    public void registering_null_name_fails() {
        var command = new RegisterPerson(mock(PersonRepository.class));
        assertThrows(NullPointerException.class, () -> command.register(null));
    }
}
