package at.deder.act.service.team.outbound.dispatch;

import at.deder.act.service.team.domain.framework.DomainEvent;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryEventDispatcherTest {

    class DummyEvent implements DomainEvent {
        public boolean dispatched = false;
    }

    @Test
    public void single_event_is_dispatched_to_subscriber() {
        var dispatcher = new InMemoryEventDispatcher();
        dispatcher.on(DummyEvent.class, e -> ((DummyEvent) e).dispatched = true);

        var event = new DummyEvent();
        dispatcher.dispatch(event);

        assertThat(event.dispatched).isTrue();
    }
}
