package at.deder.act.service.team.domain.core.team;

import at.deder.act.service.team.domain.core.people.PersonName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TeamTest {
    @Test
    public void when_team_name_is_set_the_team_has_the_name() {
        var team = createTeamWithName("team name");
        team.setName(new TeamName("team name"));
        assertThat(team.getName()).isEqualTo(new TeamName("team name"));
    }

    private Team createTeamWithName(String name) {

        return new TeamBuilder(new TeamName(name)).build();
    }

    @Test
    public void null_is_not_a_valid_team_name() {
        var team = createTeamWithName("foo");
        assertThrows(NullPointerException.class, () -> team.setName(null));
    }

    @Test
    public void team_id_is_set() {
        var team = createTeamWithName("xxx");
        team.setId("xxx");
        assertThat(team.getId()).isEqualTo("xxx");
    }

    @Test
    public void null_is_not_a_valid_id() {
        var team = createTeamWithName("foo");
        assertThrows(IllegalArgumentException.class, () -> team.setId(null));
    }

    @ParameterizedTest
    @ValueSource(strings = {"John", "mia", "Frank", "Peter Hausen"})
    public void when_member_is_added_to_team_member_is_part_of_the_team(PersonName memberName) throws DuplicateMemberException {
        var team = createTeamWithName("memberName");
        team.addMember(memberName);
        assertThat(team.isMember(memberName)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"John", "mia", "Frank", "Peter Hausen"})
    public void when_member_already_present_it_is_not_added(PersonName memberName) throws DuplicateMemberException {
        var team = createTeamWithName("memberName");
        team.addMember(memberName);
        assertThrows(DuplicateMemberException.class, () -> team.addMember(memberName));
    }

    @Test
    public void getting_notes_for_a_team_without_notes_returns_empty_list() {
        var team = createTeamWithName("foo");

        assertThat(team.getNotes()).isNotNull();
        assertThat(team.getNotes().size()).isEqualTo(0);
    }

    @Test
    public void add_note_with_content_to_team_lets_retrieve_the_note() {
        var team = createTeamWithName("foo");

        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(1234), ZoneId.of("UTC"));
        team.addNote(new Note(timestamp, "test note"));

        assertThat(team.getNotes()).isNotNull();
        assertThat(team.getNotes().size()).isEqualTo(1);

        var note = team.getNotes().get(0);
        assertThat(note.getContent()).isEqualTo("test note");
        assertThat(note.getDate().toEpochSecond(ZoneOffset.UTC)).isEqualTo(1234L);
    }

    @Test
    public void adding_an_empty_note_throws_exception() {
        var team = createTeamWithName("foo");
        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(1234), ZoneId.of("UTC"));
        assertThrows(IllegalArgumentException.class,() -> team.addNote(new Note(timestamp, "")));
    }

    @Test
    public void adding_a_blank_note_throws_exception() {
        var team = createTeamWithName("foo");
        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(1234), ZoneId.of("UTC"));
        assertThrows(IllegalArgumentException.class, () -> team.addNote(new Note(timestamp, "  ")));
    }

    @Test
    public void adding_a_null_note_throws_exception() {
        var team = createTeamWithName("foo");
        assertThrows(NullPointerException.class, () -> team.addNote(null));
    }

    @Test
    public void add_note_to_existing_notes_appends_the_new_note() {
        var team = createTeamWithName("foo");

        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(1234), ZoneId.of("UTC"));
        team.addNote(new Note(timestamp, "a"));
        timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(4321), ZoneId.of("UTC"));
        team.addNote(new Note(timestamp, "b"));

        assertThat(team.getNotes().size()).isEqualTo(2);

        var noteA = team.getNotes().get(0);
        assertThat(noteA.getContent()).isEqualTo("a");
        assertThat(noteA.getDate().toEpochSecond(ZoneOffset.UTC)).isEqualTo(1234L);

        var noteB = team.getNotes().get(1);
        assertThat(noteB.getContent()).isEqualTo("b");
        assertThat(noteB.getDate().toEpochSecond(ZoneOffset.UTC)).isEqualTo(4321L);
    }

    @Test
    public void setting_name_to_current_name_does_not_raise_name_changed_event() {
        var team = createTeamWithName("old name");
        team.setName(new TeamName("old name"));

        assertThatNotEventWasRaised(team);
    }

    @Test
    public void setting_name_for_the_first_time_does_not_raise_name_changed_event() {
        var team = createTeamWithName("first time name set");

        assertThatNotEventWasRaised(team);
    }

    @Test
    public void changing_team_name_raises_name_change_event() {
        var team = createTeamWithName("old name");
        team.setId("id");

        team.setName(new TeamName("new name"));

        var events = team.getRaisedDomainEvents();
        assertThat(events).hasSize(1);
        assertThat(events.get(0)).isInstanceOf(TeamRenamedEvent.class);

        TeamRenamedEvent renameEvent = (TeamRenamedEvent) events.get(0);
        assertThat(renameEvent.getOldName()).isEqualTo(new TeamName("old name"));
        assertThat(renameEvent.getNewName()).isEqualTo(new TeamName("new name"));
        assertThat(renameEvent.getTeamId()).isEqualTo("id");
    }

    private void assertThatNotEventWasRaised(Team team) {
        var events = team.getRaisedDomainEvents();
        assertThat(events.size()).isEqualTo(0);
    }
}
