package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class DeactivatePersonTest {
    @Test
    public void deactivating_an_existing_person_deletes_person_in_repository() {
        var repo = mock(PersonRepository.class);
        var mockPerson = mock(Person.class);
        when(repo.getPersonById("test-id")).thenReturn(mockPerson);

        var command = new DeactivatePersonCommand(repo, "test-id");
        command.execute();

        var captor = ArgumentCaptor.forClass(String.class);
        verify(repo).deletePerson(captor.capture());
        assertThat(captor.getValue()).isEqualTo("test-id");
    }

    //TODO delete non-existing person
    //TODO delete ictive person
}
