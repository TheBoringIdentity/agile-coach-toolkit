package at.deder.act.service.team.domain.query;

import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamName;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class TeamQueryServiceImplTest {

    private static final String TEST_ID = "test-id";
    private static final String TEST_NAME = "test name";

    @Test
    public void querying_all_team_names_provides_id_name_mapping_of_valid_teams() {
        TeamRepository mockRepo = mock(TeamRepository.class);
        String[][] idNamesMapping = {{"a", "b"}, {"c", "d"}};
        Map m = ArrayUtils.toMap(idNamesMapping);
        when(mockRepo.getAllValidTeamIdsAndNames()).thenReturn(m);

        var service = new TeamQueryServiceImpl(mockRepo);
        var result = service.queryTeamIdNameMapping();

        assertThat(result.size(), is(2));
        assertThat(result.get("a"), is("b"));
        assertThat(result.get("c"), is("d"));
    }

    @Test
    public void all_notes_can_be_listed_from_a_team_with_multiple_notes() {
        TeamRepository mockRepo = mock(TeamRepository.class);
        var mockTeam = mock(Team.class);
        when(mockRepo.findTeam(TEST_ID)).thenReturn(mockTeam);
        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(1234), ZoneId.of("UTC"));
        when(mockTeam.getNotes()).thenReturn(Arrays.asList(new Note(timestamp, "note A"), new Note(timestamp, "note B")));
        var service = new TeamQueryServiceImpl(mockRepo);

        var result = service.queryNotesForTeam(TEST_ID);

        assertThat(result.size(), is(2));
        assertThat(result, hasItems(new Note(timestamp, "note A"), new Note(timestamp, "note B")));
    }

    @Test
    public void querying_notes_of_not_existing_team_returns_null() {
        TeamRepository mockRepo = mock(TeamRepository.class);
        when(mockRepo.findTeam(TEST_ID)).thenReturn(null);
        var service = new TeamQueryServiceImpl(mockRepo);

        var result = service.queryNotesForTeam(TEST_ID);

        assertThat(result, nullValue());
    }

    @Test
    public void query_name_for_existing_team_returns_name() {
        var mockRepo = mock(TeamRepository.class);
        var mockTeam = mock(Team.class);

        when(mockTeam.getName()).thenReturn(new TeamName(TEST_NAME));
        when(mockRepo.findTeam(TEST_ID)).thenReturn(mockTeam);

        var service = new TeamQueryServiceImpl(mockRepo);
        var result = service.queryNameForTeam(TEST_ID);

        assertThat(result, is(new TeamName(TEST_NAME)));
        verify(mockRepo).findTeam(TEST_ID);
    }

    @Test
    public void query_name_for_not_existing_team_returns_null() {
        var mockRepo = mock(TeamRepository.class);
        when(mockRepo.findTeam(TEST_ID)).thenReturn(null);

        var service = new TeamQueryServiceImpl(mockRepo);
        var result = service.queryNameForTeam(TEST_ID);

        assertThat(result, nullValue());
        verify(mockRepo).findTeam(TEST_ID);
    }
}
