package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.query.TeamQueryService;
import at.deder.act.service.team.domain.query.TeamQueryServiceImpl;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class GetTeamsHandlerTest {
    @Test
    public void querying_team_list_returns_list_with_names() {
        TeamQueryService mockQueryService = mock(TeamQueryServiceImpl.class);
        Map<String, String> teamIdNameMapping = new HashMap<>();
        teamIdNameMapping.put("1234", "a");
        teamIdNameMapping.put("4321", "b");
        when(mockQueryService.queryTeamIdNameMapping()).thenReturn(teamIdNameMapping);

        Request mockRequest = mock(Request.class);
        Response mockResponse = mock(Response.class);


        var handler = new GetTeamsHandler(mockQueryService);
        var response = handler.handleRequest(mockRequest, mockResponse);

        verify(mockResponse).status(HttpStatus.OK_200);
        assertThat(response, isJson());
        assertThat(response, hasJsonPath("$.1234", is("a")));
        assertThat(response, hasJsonPath("$.4321", is("b")));
    }
}
