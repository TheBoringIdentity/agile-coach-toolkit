package at.deder.act.service.team.outbound.audit;

import at.deder.act.service.team.domain.core.team.TeamCreatedEvent;
import at.deder.act.service.team.domain.core.team.TeamName;
import at.deder.act.service.team.domain.core.team.TeamRenamedEvent;
import at.deder.act.service.team.outbound.dispatch.InMemoryEventDispatcher;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AuditLogServiceTest {
    @Test
    public void create_log_entry_when_team_is_renamed() {
        var event = new TeamRenamedEvent("id", new TeamName("old name"), new TeamName("new name"));

        var eventDispatch = new InMemoryEventDispatcher();
        var auditLog = mock(AuditLogAdapter.class);
        var service = new AuditLogService(eventDispatch, auditLog);
        service.start();

        eventDispatch.dispatch(event);

        verify(auditLog).writeAuditLog(anyString()); // TODO weak test
    }

    @Test
    public void create_log_entry_when_team_is_created() {
        var event = new TeamCreatedEvent("id");

        var auditLog = mock(AuditLogAdapter.class);
        var eventDispatch = new InMemoryEventDispatcher();
        var service = new AuditLogService(eventDispatch, auditLog);
        service.start();

        eventDispatch.dispatch(event);

        verify(auditLog).writeAuditLog(anyString()); // TODO weak test
    }
}
