package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import spark.Request;
import spark.Response;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class AppendNoteHandlerTest {
    @Test
    public void handler_call_executes_append_note_command() {
        var mockRepository = mock(TeamRepository.class);
        var mockRequest = mock(Request.class);
        var mockResponse = mock(Response.class);
        var mockTeam = mock(Team.class);

        when(mockRequest.body()).thenReturn("{\"content\": \"test content\"}");
        when(mockRequest.params("id")).thenReturn("test-team");
        when(mockRepository.findTeam("test-team")).thenReturn(mockTeam);

        var handler = new AppendNoteHandler(mockRepository);
        handler.handle(mockRequest, mockResponse);

        var captor = ArgumentCaptor.forClass(Note.class);
        verify(mockTeam).addNote(captor.capture());
        assertThat(captor.getValue().getContent(), is("test content"));
        verify(mockResponse).status(HttpStatus.OK_200);
    }

    @Test
    public void handler_call_with_multiple_notes_appends_multiple_notes_at_once() {
        var mockRepository = mock(TeamRepository.class);
        var mockRequest = mock(Request.class);
        var mockResponse = mock(Response.class);
        var mockTeam = mock(Team.class);

        when(mockRequest.body()).thenReturn("{\"content\": [\"test content A\", \"test content B\"]}");
        when(mockRequest.params("id")).thenReturn("test-team");
        when(mockRepository.findTeam("test-team")).thenReturn(mockTeam);

        var handler = new AppendNoteHandler(mockRepository);
        handler.handle(mockRequest, mockResponse);

        var captor = ArgumentCaptor.forClass(Note.class);
        verify(mockTeam, times(2)).addNote(captor.capture());

        var contents = captor.getAllValues();
        assertThat(contents.size(), is(2));
        assertThat(contents.get(0).getContent(), is("test content A"));
        assertThat(contents.get(1).getContent(), is("test content B"));

        verify(mockResponse).status(HttpStatus.OK_200);
    }
}
