package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class AddNoteToTeamTest {

    private TeamRepository mockRepository;
    private Team mockTeam;

    @Test
    public void adding_a_valid_note_adds_a_note_to_the_team() {
        prepareMocks();

        var command = prepareCommand();
        command.addNote("test content");
        command.submit();

        verifyThatNoteWasAddedToTeam(mockRepository, mockTeam, "test content");
    }

    @Test
    public void adding_multiple_notes_adds_each_note_to_the_team() {
        prepareMocks();

        var command = prepareCommand();
        command.addNote("content 1");
        command.addNote("content 2");
        command.addNote("content 3");
        command.submit();

        verifyThatNoteWasAddedToTeam(mockRepository, mockTeam, "content 1", "content 2", "content 3");
    }

    @Test
    public void adding_invalid_comment_fails() {
        prepareMocks();

        var command = prepareCommand();
        assertThrows(NullPointerException.class, () -> command.addNote(null));
        assertThrows(IllegalArgumentException.class, () -> command.addNote(""));
        assertThrows(IllegalArgumentException.class, () -> command.addNote("             "));
    }

    private void verifyThatNoteWasAddedToTeam(TeamRepository mockRepository, Team mockTeam, String... notes) {
        verify(mockRepository).findTeam("team-id");
        var captor = ArgumentCaptor.forClass(Note.class);
        verify(mockTeam, times(notes.length)).addNote(captor.capture());

        var captorValues = captor.getAllValues().stream().map(Note::getContent).collect(Collectors.toList());
        for(String note: notes) {
            assertThat(captorValues, hasItem(note));
        }
        verify(mockRepository).saveTeam(mockTeam);
    }

    private AddNoteToTeam prepareCommand() {
        return new AddNoteToTeam(mockRepository, "team-id");
    }

    private void prepareMocks() {
        mockRepository = mock(TeamRepository.class);
        mockTeam = mock(Team.class);
        when(mockRepository.findTeam("team-id")).thenReturn(mockTeam);
    }
}
