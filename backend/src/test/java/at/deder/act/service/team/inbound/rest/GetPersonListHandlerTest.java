package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.query.PersonQueryService;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import java.util.Arrays;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.mockito.Mockito.*;

public class GetPersonListHandlerTest {
    @Test
    public void all_registered_persons_are_returned() {
        var mockQueryService = mock(PersonQueryService.class);
        var personA = new Person();
        personA.setId("a");
        personA.setName(new PersonName("Max"));
        var personB = new Person();
        personB.setId("b");
        personB.setName(new PersonName("Nina"));
        when(mockQueryService.queryAllPeople()).thenReturn(Arrays.asList(personA, personB));

        var mockRequest = mock(Request.class);
        var mockResponse = mock(Response.class);

        var handler = new GetPersonListHandler(mockQueryService);
        var result = handler.handle(mockRequest, mockResponse);

        verify(mockResponse).status(HttpStatus.OK_200);
        assertThatJson(result).isArray().hasSize(2);
        assertThatJson(result).inPath("$[0].name.name").isEqualTo("Max");
        assertThatJson(result).inPath("$[0].id").isEqualTo("a");
        assertThatJson(result).inPath("$[1].name.name").isEqualTo("Nina");
        assertThatJson(result).inPath("$[1].id").isEqualTo("b");
    }
}
