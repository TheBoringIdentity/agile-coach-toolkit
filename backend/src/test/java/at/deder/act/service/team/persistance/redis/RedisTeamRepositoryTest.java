package at.deder.act.service.team.persistance.redis;

import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamBuilder;
import at.deder.act.service.team.domain.core.team.TeamName;
import at.deder.act.service.team.persistance.TeamDoesNotExist;
import at.deder.act.service.team.persistance.IdGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static at.deder.act.service.team.persistance.redis.RedisTeamRepository.KEY_VALID;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class RedisTeamRepositoryTest {
    @Test
    public void team_with_name_and_id_can_be_stored() {
        var originalTeam = createTeamWithName("test name");
        originalTeam.setName(new TeamName("test name"));
        originalTeam.setId("x");

        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.isMemberOfSet(KEY_VALID, "x")).thenReturn(true);

        var idGenerator = mock(IdGenerator.class);

        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, idGenerator);

        adapter.saveTeam(originalTeam);

        verify(mockJedis).set(String.format("team:%s:name", originalTeam.getId()), "test name");
    }

    private Team createTeamWithName(String name) {
        return new TeamBuilder(new TeamName(name)).build();
    }

    @Test
    public void restored_team_retrieves_name() {
        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.get("team:test:name")).thenReturn("test name");
        when(mockJedis.isMemberOfSet(KEY_VALID, "test")).thenReturn(true);
        var adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));

        var foundTeam = adapter.findTeam("test");
        assertThat(foundTeam, notNullValue());
        assertThat(foundTeam.getName(), is(new TeamName("test name")));
    }

    @Test
    public void restored_team_retrieves_stored_notes() {
        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.get("team:test:name")).thenReturn("test name");

        when(mockJedis.get("team:test:notes")).thenReturn("[{\"content\":\"a\",\"timestamp\":1234},{\"content\":\"b\",\"timestamp\":1234}]");
        when(mockJedis.isMemberOfSet(KEY_VALID, "test")).thenReturn(true);
        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));

        var foundTeam = adapter.findTeam("test");
        assertThat(foundTeam, notNullValue());

        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(1234), ZoneId.of("UTC"));
        assertThat(foundTeam.getNotes(), hasItems(new Note(timestamp, "a"), new Note(timestamp, "b")));
    }

    @Test
    public void restored_team_retrieves_stored_notes_when_timestamp_in_redis_is_missing() {
        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.get("team:test:name")).thenReturn("test name");
        when(mockJedis.get("team:test:notes")).thenReturn("[{\"content\":\"a\"},{\"content\":\"b\"}]");
        when(mockJedis.isMemberOfSet(KEY_VALID, "test")).thenReturn(true);
        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));

        var foundTeam = adapter.findTeam("test");
        assertThat(foundTeam, notNullValue());

        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(0), ZoneId.of("UTC"));
        assertThat(foundTeam.getNotes(), hasItems(new Note(timestamp, "a"), new Note(timestamp, "b")));
    }

    @Test
    public void restored_team_retrieves_stored_notes_when_note_is_only_string_compatibility() {
        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.get("team:test:name")).thenReturn("test name");
        when(mockJedis.get("team:test:notes")).thenReturn("[\"a\",\"b\"]");
        when(mockJedis.isMemberOfSet(KEY_VALID, "test")).thenReturn(true);
        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));

        var foundTeam = adapter.findTeam("test");
        assertThat(foundTeam, notNullValue());

        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(0), ZoneId.of("UTC"));
        assertThat(foundTeam.getNotes(), hasItems(new Note(timestamp, "a"), new Note(timestamp, "b")));
    }

    @Test
    public void restoring_a_nonexistant_team_returns_null() {
        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.isMemberOfSet(KEY_VALID, "test")).thenReturn(false);

        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));

        var foundTeam = adapter.findTeam("test");

        assertThat(foundTeam, nullValue());
    }

    @Test
    public void createTeam_returns_team_with_id() {
        var mockJedis = mock(RedisConnection.class);
        var mockIdGenerator = mock(IdGenerator.class);
        when(mockIdGenerator.generateId()).thenReturn("test-id");
        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, mockIdGenerator);

        var createdTeam = adapter.createTeam("foo");

        assertThat(createdTeam.getId(), is("test-id"));
    }

    @Test
    public void updateName_on_nonexisting_team_fails() {
        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.get("team:nonexistant:valid")).thenReturn(null);

        Team mockTeam = mock(Team.class);
        when(mockTeam.getId()).thenReturn("nonexistant");
        when(mockTeam.getName()).thenReturn(new TeamName("foo"));

        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));

        assertThrows(TeamDoesNotExist.class, () -> adapter.saveTeam(mockTeam));
    }

    @Test
    public void createTeam_sets_team_to_valid() {
        var mockJedis = mock(RedisConnection.class);
        var mockIdGenerator = mock(IdGenerator.class);

        when(mockIdGenerator.generateId()).thenReturn("test-id");

        RedisTeamRepository adapter = new RedisTeamRepository(mockJedis, mockIdGenerator);

        var createdTeam = adapter.createTeam("foo");

        verify(mockJedis).addToSet(KEY_VALID, createdTeam.getId());
    }

    @Test
    public void valid_teams_are_listed_when_queried() {
        var mockJedis = mock(RedisConnection.class);

        when(mockJedis.getMembersOfSet(KEY_VALID)).thenReturn(Stream.of("a", "b", "c").collect(Collectors.toSet()));
        when(mockJedis.get("team:a:name")).thenReturn("1");
        when(mockJedis.get("team:b:name")).thenReturn("2");
        when(mockJedis.get("team:c:name")).thenReturn("3");

        var adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));
        var listedTeams = adapter.getAllValidTeamIdsAndNames();

        assertThat(listedTeams.size(), is(3));
        assertThat(listedTeams.keySet(), hasItems("a", "b", "c"));
        assertThat(listedTeams.get("a"), is("1"));
        assertThat(listedTeams.get("b"), is("2"));
        assertThat(listedTeams.get("c"), is("3"));
    }

    @Test
    public void saving_a_team_saves_its_notes() {
        var mockJedis = mock(RedisConnection.class);
        when(mockJedis.isMemberOfSet(KEY_VALID, "test-id")).thenReturn(true);

        var adapter = new RedisTeamRepository(mockJedis, mock(IdGenerator.class));
        var team = createTeamWithName("foo");
        team.setId("test-id");

        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(1234), ZoneId.of("UTC"));
        team.addNote(new Note(timestamp, "test note"));
        team.addNote(new Note(timestamp,"test note 2"));
        adapter.saveTeam(team);

        var notesContentCaptor = ArgumentCaptor.forClass(String.class);
        var keyCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockJedis, atLeast(1)).set(keyCaptor.capture(), notesContentCaptor.capture());

        var noteJson = notesContentCaptor.getValue();
        assertThat(noteJson, hasJsonPath("$[0].content", is("test note")));
        assertThat(noteJson, hasJsonPath("$[0].timestamp", is(1234)));
        assertThat(noteJson, hasJsonPath("$[1].content", is("test note 2")));
        assertThat(noteJson, hasJsonPath("$[1].timestamp", is(1234)));
        assertThat(keyCaptor.getValue(), is("team:test-id:notes"));
    }
}
