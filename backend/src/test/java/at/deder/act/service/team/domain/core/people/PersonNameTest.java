package at.deder.act.service.team.domain.core.people;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersonNameTest {
    @Test
    public void non_empty_string_is_a_valid_name() {
        var name = new PersonName("test name");
        assertThat(name.asString()).isEqualTo("test name");
    }

    @Test
    public void null_is_not_a_valid_name() {
        assertThrows(NullPointerException.class, () -> new PersonName(null));
    }

    @Test
    public void empty_string_is_not_a_valid_name() {
        assertThrows(IllegalArgumentException.class, () -> new PersonName(""));
        assertThrows(IllegalArgumentException.class, () -> new PersonName("    "));
    }
}
