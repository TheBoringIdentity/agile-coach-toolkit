package at.deder.act.service.team.domain.framework;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EntityTest {

    @Test
    public void entity_can_raise_events_to_be_processed_later() {
        var entity = new Entity(){};
        var eventTobeFired = new DomainEvent(){};

        entity.raiseEvent(eventTobeFired);

        assertThat(entity.getRaisedDomainEvents()).contains(eventTobeFired);
    }
}
