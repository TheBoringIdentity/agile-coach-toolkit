package at.deder.act.service.team.persistance;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RandomUUIdIdGeneratorTest {
    @Test
    public void generated_id_contains_valid_characters_only() {
        var generator = new RandomUUIDIdGenerator();

        var generatedId = generator.generateId();

        assertThat(generatedId).matches("[A-Za-z0-9\\-]+");
    }
}