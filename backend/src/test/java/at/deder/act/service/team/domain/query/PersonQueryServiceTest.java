package at.deder.act.service.team.domain.query;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PersonQueryServiceTest {
    @Test
    public void queryAllPeople_returns_all_registered_people() {
        var mockRepository = mock(PersonRepository.class);
        var mockPersonA = mock(Person.class);
        var mockPersonB = mock(Person.class);
        when(mockRepository.queryAllPersonIds()).thenReturn(Arrays.asList("a", "b"));
        when(mockRepository.getPersonById("a")).thenReturn(mockPersonA);
        when(mockRepository.getPersonById("b")).thenReturn(mockPersonB);

        var queryService = new PersonQueryService(mockRepository);
        var result = queryService.queryAllPeople();

        assertThat(result).containsExactlyInAnyOrder(mockPersonA, mockPersonB);
    }
}
