package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.query.TeamQueryServiceImpl;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class GetTeamNotesHandlerTest {
    @Test
    public void retrieving_team_notes_for_existing_team_with_notes() {
        var mockQueryService = mock(TeamQueryServiceImpl.class);
        var mockRequest = mock(Request.class);
        var mockResponse = mock(Response.class);

        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(1234), ZoneId.of("UTC"));
        when(mockQueryService.queryNotesForTeam("test-id")).thenReturn(Arrays.asList(new Note(timestamp, "a"), new Note(timestamp, "b")));
        when(mockRequest.params("id")).thenReturn("test-id");

        var handler = new GetTeamNotesHandler(mockQueryService);
        var result = handler.handle(mockRequest, mockResponse);

        verify(mockResponse).status(HttpStatus.OK_200);
        verify(mockQueryService).queryNotesForTeam("test-id");
        assertThat(result, isJson());
        assertThat(result, hasJsonPath("$[0].content", is("a")));
        assertThat(result, hasJsonPath("$[0].timestamp", is(1234)));
        assertThat(result, hasJsonPath("$[1].content", is("b")));
        assertThat(result, hasJsonPath("$[1].timestamp", is(1234)));
    }

    @Test
    public void getting_notes_for_nonexistant_team_fails_with_not_found() {
        var mockQueryService = mock(TeamQueryServiceImpl.class);
        var mockRequest = mock(Request.class);
        var mockResponse = mock(Response.class);

        when(mockQueryService.queryNotesForTeam("test-id")).thenReturn(null);
        when(mockRequest.params("id")).thenReturn("test-id");

        var handler = new GetTeamNotesHandler(mockQueryService);
        handler.handle(mockRequest, mockResponse);

        verify(mockQueryService).queryNotesForTeam("test-id");
        verify(mockResponse).status(HttpStatus.NOT_FOUND_404);

    }

    @Test
    public void getting_notes_for_existing_team_without_notes_returns_empty_list() {
        var mockQueryService = mock(TeamQueryServiceImpl.class);
        var mockRequest = mock(Request.class);
        var mockResponse = mock(Response.class);

        when(mockQueryService.queryNotesForTeam("test-id")).thenReturn(new ArrayList<>());
        when(mockRequest.params("id")).thenReturn("test-id");

        var handler = new GetTeamNotesHandler(mockQueryService);
        var result = handler.handle(mockRequest, mockResponse);

        verify(mockResponse).status(HttpStatus.OK_200);
        verify(mockQueryService).queryNotesForTeam("test-id");
        assertThat(result, is("[]"));
    }
}
