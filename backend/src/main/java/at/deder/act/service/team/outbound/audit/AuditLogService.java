package at.deder.act.service.team.outbound.audit;

import at.deder.act.service.team.domain.core.team.TeamCreatedEvent;
import at.deder.act.service.team.domain.core.team.TeamRenamedEvent;
import at.deder.act.service.team.domain.framework.DomainEvent;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import com.google.gson.Gson;
import com.google.inject.Inject;

public class AuditLogService {
    private DomainEventDispatcher eventDispatch;
    private final AuditLogAdapter outstream;

    @Inject
    AuditLogService(DomainEventDispatcher eventDispatch, AuditLogAdapter outstream) {
        this.eventDispatch = eventDispatch;
        this.outstream = outstream;
    }

    private void logEvent(DomainEvent event) {
        var gson = new Gson();
        var json = gson.toJsonTree(event);
        json.getAsJsonObject().addProperty("_type", event.getClass().getCanonicalName());
        outstream.writeAuditLog(json.toString());
    }

    public void start() {
        eventDispatch.on(TeamRenamedEvent.class, this::logEvent);
        eventDispatch.on(TeamCreatedEvent.class, this::logEvent);
    }
}
