package at.deder.act.service.team.domain.query;

import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.TeamName;

import java.util.List;
import java.util.Map;

public interface TeamQueryService {
    Map<String, String> queryTeamIdNameMapping();

    List<Note> queryNotesForTeam(String id);

    TeamName queryNameForTeam(String id);
}
