package at.deder.act.service.team.persistance;

public interface IdGenerator {
    String generateId();
}
