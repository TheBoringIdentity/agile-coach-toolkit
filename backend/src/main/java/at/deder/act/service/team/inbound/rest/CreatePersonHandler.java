package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.command.RegisterPerson;
import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import static spark.Spark.halt;

public class CreatePersonHandler {
    private PersonRepository repo;

    @Inject
    public CreatePersonHandler(PersonRepository repo) {
        this.repo = repo;
    }

    public String handle(Request request, Response response) {
        String name = extractNameFromBody(request);
        Person person = registerPersonByName(name);
        JsonObject json = buildResponseContent(response, person);
        return json.toString();
    }

    private JsonObject buildResponseContent(Response response, Person person) {
        response.status(HttpStatus.OK_200);
        var json = new JsonObject();
        json.addProperty("id", person.getId());
        return json;
    }

    private Person registerPersonByName(String name) {
        var command = new RegisterPerson(repo);
        PersonName nameObj = stringToName(name);
        return command.register(nameObj);
    }

    private String extractNameFromBody(Request request) {
        var body = request.body();
        if(body.isEmpty()) {
            haltWithBadRequest("Missing payload", "This call requires the name for the new person.");
        }

        var root = new JsonParser().parse(body).getAsJsonObject();
        var nameProperty = root.get("name");
        if(nameProperty == null) {
            haltWithBadRequest("Missing payload", "This call requires the name for the new person.");
        }

        return nameProperty.getAsString();
    }

    private void haltWithBadRequest(String s, String s2) {
        JsonObject errorJson = buildErrorJson(s, s2);
        halt(HttpStatus.BAD_REQUEST_400, errorJson.toString());
    }

    private JsonObject buildErrorJson(String title, String detail) {
        var errorJson = new JsonObject();
        errorJson.addProperty("title", title);
        errorJson.addProperty("detail", detail);
        return errorJson;
    }

    private PersonName stringToName(String name) {
        PersonName nameObj = null;
        try {
            nameObj = new PersonName(name);
        } catch(IllegalArgumentException e) {
            haltWithBadRequest("Malformed person name", e.getMessage());
        }
        return nameObj;
    }
}
