package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import org.apache.commons.lang3.Validate;

public class RegisterPerson {
    private PersonRepository mockRepo;

    public RegisterPerson(PersonRepository mockRepo) {
        this.mockRepo = mockRepo;
    }

    public Person register(PersonName personName) {
        Validate.notNull(personName);

        var person = mockRepo.createPerson();
        person.setName(personName);

        mockRepo.savePerson(person);
        return person;
    }
}
