package at.deder.act.service.team.persistance.redis;

import redis.clients.jedis.Jedis;

import java.util.Set;

public class RedisAdapter implements RedisConnection {

    private final Jedis jedis;

    RedisAdapter() {
        this.jedis = new Jedis();
    }

    @Override
    public void selectDatabase(int db) {
        jedis.select(db);

    }

    @Override
    public void set(String key, String value) {
        jedis.set(key, value);
    }

    @Override
    public void addToSet(String key, String value) {
        jedis.sadd(key, value);
    }

    @Override
    public Set<String> getMembersOfSet(String key) {
        return jedis.smembers(key);
    }

    @Override
    public String get(String key) {
        return jedis.get(key);
    }

    @Override
    public boolean isMemberOfSet(String keyValid, String value) {
        return jedis.sismember(keyValid, value);
    }
}
