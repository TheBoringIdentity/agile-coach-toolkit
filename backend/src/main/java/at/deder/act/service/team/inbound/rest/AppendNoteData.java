package at.deder.act.service.team.inbound.rest;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class AppendNoteData extends AbstractRequestData {
    private String id;
    private List<String> content;

    static AppendNoteData parse(Request request, Response response) {
        var data = new AppendNoteData();

        var id = request.params("id");
        if(data.responseInvalidWhenIdIsNotValid(response, id)) {
            return data;
        }

        var body = request.body();
        if(body == null) {
            data.invalidate(response);
            return data;
        }

        JsonObject root = convertFromJson(request);
        if(root == null) {
            data.invalidate(response);
            return data;
        }

        var jsonContent = root.get("content");
        if(jsonContent == null) {
            data.invalidate(response);
            return data;
        }

        setData(data, id, jsonContent);
        return data;
    }

    private static void setData(AppendNoteData data, String id, JsonElement jsonContent) {
        data.id = id;
        extractContentBasedOnType(data, jsonContent);
        data.setValid();
    }

    private static void extractContentBasedOnType(AppendNoteData data, JsonElement jsonContent) {
        if(jsonContent.isJsonArray()) {
            extractContentFromArray(data, jsonContent);
        } else {
            extractContentFromString(data, jsonContent);
        }
    }

    private static void extractContentFromString(AppendNoteData data, JsonElement jsonContent) {
        var content = jsonContent.getAsString();
        data.content = Arrays.asList(content);
    }

    private static void extractContentFromArray(AppendNoteData data, JsonElement jsonContent) {
        var elements = jsonContent.getAsJsonArray();
        data.content = new ArrayList<>();
        for(JsonElement e: elements) {
            data.content.add(e.getAsString());
        }
    }

    private boolean responseInvalidWhenIdIsNotValid(Response response, String id) {
        if(id == null) {
            invalidate(response);
            return true;
        }
        if(id.isBlank()) {
            invalidate(response);
            return true;
        }
        return false;
    }

    public String getId() {
        return id;
    }

    List<String> getNotesToAdd() {
        return content;
    }
}
