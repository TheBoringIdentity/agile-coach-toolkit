package at.deder.act.service.team.domain.framework;

import java.util.List;
import java.util.function.Consumer;

public interface DomainEventDispatcher {
    void dispatch(DomainEvent event);
    void on(Class<? extends DomainEvent> type, Consumer<DomainEvent> function);
    void dispatchAll(List<DomainEvent> raisedDomainEvents);
}
