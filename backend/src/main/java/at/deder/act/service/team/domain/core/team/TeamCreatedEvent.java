package at.deder.act.service.team.domain.core.team;

import at.deder.act.service.team.domain.framework.DomainEvent;

public class TeamCreatedEvent implements DomainEvent {
    private final String id;

    public TeamCreatedEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
