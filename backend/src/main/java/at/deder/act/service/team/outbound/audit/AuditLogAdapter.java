package at.deder.act.service.team.outbound.audit;

public interface AuditLogAdapter {
    void writeAuditLog(String record);
}
