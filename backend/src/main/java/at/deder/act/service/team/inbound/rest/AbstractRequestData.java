package at.deder.act.service.team.inbound.rest;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

abstract class AbstractRequestData {
    private boolean valid;

    static JsonObject convertFromJson(Request request) {
        return new JsonParser().parse(request.body()).getAsJsonObject();
    }

    void invalidate(Response response) {
        response.status(HttpStatus.BAD_REQUEST_400);
        valid = false;
    }

    void setValid() {
        valid = true;
    }

    boolean isValid() {
        return valid;
    }
}
