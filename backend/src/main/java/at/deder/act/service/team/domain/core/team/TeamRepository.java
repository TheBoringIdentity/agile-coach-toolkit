package at.deder.act.service.team.domain.core.team;

import java.util.Map;

public interface TeamRepository {
    Team findTeam(String id);
    void saveTeam(Team team);
    Team createTeam(String name);

    Map<String, String> getAllValidTeamIdsAndNames();
}
