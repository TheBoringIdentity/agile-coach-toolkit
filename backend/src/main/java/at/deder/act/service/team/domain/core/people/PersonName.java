package at.deder.act.service.team.domain.core.people;

import org.apache.commons.lang3.Validate;

import java.util.Objects;

public final class PersonName {
    private final String name;

    public PersonName(String name) {
        Validate.notBlank(name);
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonName name1 = (PersonName) o;
        return name.equals(name1.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String asString() {
        return name;
    }
}
