package at.deder.act.service.team.persistance;

import at.deder.act.service.team.domain.core.team.Note;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.*;

public class NoteJsonAdapter implements JsonSerializer<Note>, JsonDeserializer<Note> {

    @Override
    public JsonElement serialize(Note note, Type t, JsonSerializationContext jsc) {
        var json = new JsonObject();
        json.addProperty("content", note.getContent());
        json.addProperty("timestamp", note.getDate().toEpochSecond(ZoneOffset.UTC));
        return json;
    }

    @Override
    public Note deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext jsc) throws JsonParseException {
        if (json.isJsonObject()) {
            return deserializeJsonObjectToNote(json);
        }
        return deserializePrimitiveToNote(json);
    }

    private Note deserializePrimitiveToNote(JsonElement json) {
        var content = json.getAsString();
        var timestamp = LocalDateTime.ofInstant(Instant.ofEpochSecond(0), ZoneOffset.UTC);
        return new Note(timestamp, content);
    }

    private Note deserializeJsonObjectToNote(JsonElement json) {
        var obj = json.getAsJsonObject();
        var content = obj.get("content");
        LocalDateTime timestamp = calculateEpochSecondsForNoteJson(obj);
        return new Note(timestamp, content.getAsString());
    }

    private LocalDateTime calculateEpochSecondsForNoteJson(JsonObject obj) {
        var jsonTimestampEntry = obj.get("timestamp");
        long epochSeconds;
        epochSeconds = convertJsonEntryToLongOrZero(jsonTimestampEntry);
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(epochSeconds), ZoneOffset.UTC);
    }

    private long convertJsonEntryToLongOrZero(JsonElement entry) {
        long value;
        if (entry != null) {
            value = entry.getAsLong();
        } else {
            value = 0;
        }
        return value;
    }
}
