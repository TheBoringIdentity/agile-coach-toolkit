package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamCreatedEvent;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.inject.Inject;

public class RegisterNewTeam {
    private final TeamRepository repo;
    private final String name;
    private final DomainEventDispatcher eventDispatcher;

    @Inject
    public RegisterNewTeam(TeamRepository repo, String name, DomainEventDispatcher eventDispatcher) {
        this.repo = repo;
        this.name = name;
        this.eventDispatcher = eventDispatcher;
    }

    public Team register() {
        var team = repo.createTeam(name);
        repo.saveTeam(team);

        team.getRaisedDomainEvents().forEach(eventDispatcher::dispatch);
        eventDispatcher.dispatch(new TeamCreatedEvent(team.getId()));

        return team;
    }
}
