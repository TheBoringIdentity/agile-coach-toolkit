package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.inject.Inject;
import org.apache.commons.lang3.Validate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AddNoteToTeam {
    private final String teamId;
    private TeamRepository mockRepository;
    private List<Note> content;

    @Inject
    public AddNoteToTeam(TeamRepository repository, String teamId) {
        this.mockRepository = repository;
        this.teamId = teamId;
        this.content = new ArrayList<>();
    }

    public void addNote(String content) {
        Validate.notBlank(content);
        this.content.add(new Note(LocalDateTime.now(), content));
    }

    public void submit() {
        var team = this.mockRepository.findTeam(teamId);
        this.content.forEach(team::addNote);
        this.mockRepository.saveTeam(team);
    }
}
