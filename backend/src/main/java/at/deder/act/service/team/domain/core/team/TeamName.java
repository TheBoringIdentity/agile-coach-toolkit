package at.deder.act.service.team.domain.core.team;

import org.apache.commons.lang3.Validate;

import java.util.Objects;

public class TeamName {
    private final String name;

    public TeamName(String name) {
        Validate.notBlank(name);
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamName name1 = (TeamName) o;
        return name.equals(name1.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String asString() {
        return name;
    }
}
