package at.deder.act.service.team.persistance.redis;

import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamName;
import at.deder.act.service.team.persistance.NoteJsonAdapter;
import at.deder.act.service.team.persistance.TeamDoesNotExist;
import at.deder.act.service.team.persistance.IdGenerator;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RedisTeamRepository implements TeamRepository {
    private static final String KEY_NAME = "name";
    static final String KEY_VALID = "team:VALID";

    private RedisConnection redis;
    private IdGenerator idGenerator;

    @Inject
    RedisTeamRepository(RedisConnection jedisConnection, IdGenerator idGenerator) {
        redis = jedisConnection;
        this.idGenerator = idGenerator;
        redis.selectDatabase(0);
    }

    @Override
    synchronized public void saveTeam(Team team) {
        var id = team.getId();
        if(isTeamInvalid(id)) {
            throw new TeamDoesNotExist();
        }

        redis.set(keyForId(id, KEY_NAME), team.getName().asString());

        var gson = createGsonBuilder();
        var serialisedNotes = gson.toJson(team.getNotes());
        redis.set(keyForId(id, "notes"), serialisedNotes);
    }

    private Gson createGsonBuilder() {
        return new GsonBuilder().registerTypeAdapter(Note.class, new NoteJsonAdapter()).create();
    }

    @Override
    synchronized public Team createTeam(String name) {
        var id = idGenerator.generateId();
        redis.addToSet(KEY_VALID, id);

        var team = Team.create(new TeamName(name));
        team.setId(id);

        return team;
    }

    @Override
    synchronized public Map<String, String> getAllValidTeamIdsAndNames() {
        Set<String> validSetIds = redis.getMembersOfSet(KEY_VALID);

        Map idNameTable = new HashMap<String, String>();
        validSetIds.forEach(id -> idNameTable.put(id, getNameForId(id)));

        return idNameTable;
    }

    private String getNameForId(String id) {
        return redis.get(keyForId(id, KEY_NAME));
    }

    @Override
    synchronized public Team findTeam(String idToFind) {
        if(isTeamInvalid(idToFind)) {
            return null;
        }

        var name = getNameForId(idToFind);
        var team = Team.create(new TeamName(name));
        team.setId(idToFind);

        setNameForTeam(team);
        setNotesForTeam(team);

        return team;
    }

    private void setNotesForTeam(Team team) {
        var notesJson = redis.get(keyForId(team.getId(), "notes"));
        if(notesJson != null) {
            Type typeToken = new TypeToken<List<Note>>() {}.getType();
            var gson = createGsonBuilder();
            List<Note> notes = gson.fromJson(notesJson, typeToken);
            notes.forEach(team::addNote);
        }
    }

    private void setNameForTeam(Team team) {
        var name = getNameForId(team.getId());
        team.setName(new TeamName(name));
    }

    private boolean isTeamInvalid(String id) {
        boolean valid = redis.isMemberOfSet(KEY_VALID, id);
        return !valid;
    }

    private String keyForId(String id, String key) {
        return String.format("team:%s:%s", id, key);
    }
}
