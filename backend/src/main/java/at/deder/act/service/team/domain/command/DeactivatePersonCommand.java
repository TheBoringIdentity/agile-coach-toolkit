package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.PersonRepository;

public class DeactivatePersonCommand {
    private final PersonRepository repository;
    private String personId;

    public DeactivatePersonCommand(PersonRepository repository, String personId) {
        this.repository = repository;
        this.personId = personId;
    }

    public void execute() {
        repository.deletePerson(personId);
    }
}
