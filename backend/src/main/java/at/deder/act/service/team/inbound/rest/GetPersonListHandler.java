package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.query.PersonQueryService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

public class GetPersonListHandler {
    private PersonQueryService queryService;

    @Inject
    public GetPersonListHandler(PersonQueryService queryService) {
        this.queryService = queryService;
    }

    public String handle(final Request request, final Response response) {
        var allPeople = queryService.queryAllPeople();
        var gson = new Gson();
        var json = gson.toJson(allPeople);
        response.status(HttpStatus.OK_200);
        return json;
    }
}
