package at.deder.act.service.team.domain.core.team;

import at.deder.act.service.team.domain.framework.Entity;
import org.apache.commons.lang3.Validate;
import at.deder.act.service.team.domain.core.people.PersonName;

import java.util.ArrayList;
import java.util.List;

public class Team extends Entity {
    List<PersonName> members = null;
    private TeamName name;
    private String id;
    private List<Note> notes;

    private Team() {
        notes = new ArrayList<>();
    }

    public static Team create(TeamName name) {
        var team = new Team();
        team.setName(name);

        return team;
    }



    public void addMember(PersonName name)  {
        if(members == null) {
            members = new ArrayList<PersonName>();
        }

        if(members.contains(name)) {
            throw new DuplicateMemberException();
        }

        members.add(name);
    }

    public boolean isMember(PersonName memberName) {
        return members.contains(memberName);
    }

    public void setName(TeamName name) {
        Validate.notNull(name);

        raiseNameChangeEventIfTeamNameChanged(name);

        this.name = name;
    }

    private void raiseNameChangeEventIfTeamNameChanged(TeamName name) {
        if(this.name != null && !name.equals(this.name)) {
            raiseEvent(new TeamRenamedEvent(id, this.name, name));
        }
    }

    public TeamName getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        if(id == null) {
            throw new IllegalArgumentException();
        }
        this.id = id;
    }

    public void addNote(Note note) {
        Validate.notBlank(note.getContent());
        notes.add(note);
    }

    public List<Note> getNotes() {
        return notes;
    }
}
