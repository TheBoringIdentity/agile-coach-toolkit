package at.deder.act.service.team.domain.query;

import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.domain.core.team.TeamName;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.inject.Inject;

import java.util.List;
import java.util.Map;

public class TeamQueryServiceImpl implements TeamQueryService {
    private TeamRepository repository;

    @Inject
    public TeamQueryServiceImpl(TeamRepository repository) {
        this.repository = repository;
    }

    @Override
    public Map<String, String> queryTeamIdNameMapping() {
        return repository.getAllValidTeamIdsAndNames();
    }

    @Override
    public List<Note> queryNotesForTeam(String id) {
        var team = repository.findTeam(id);
        if(team == null) {
            return null;
        }

        return team.getNotes();
    }

    @Override
    public TeamName queryNameForTeam(String id) {
        var team = repository.findTeam(id);
        if(team == null) {
            return null;
        }
        return team.getName();
    }
}
