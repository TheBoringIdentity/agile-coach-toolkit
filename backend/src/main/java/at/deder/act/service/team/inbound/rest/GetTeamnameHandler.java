package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.query.TeamQueryService;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

public class GetTeamnameHandler {
    private TeamQueryService queryService;

    @Inject
    public GetTeamnameHandler(TeamQueryService queryService) {
        this.queryService = queryService;
    }

    public String handle(Request request, Response response) {
        var id = request.params("id");
        if(id == null) {
            response.status(HttpStatus.BAD_REQUEST_400);
            return null;
        }

        var name = queryService.queryNameForTeam(id);
        if(name == null) {
            response.status(HttpStatus.NOT_FOUND_404);
            return null;
        }

        response.status(HttpStatus.OK_200);
        return name.asString();
    }
}
