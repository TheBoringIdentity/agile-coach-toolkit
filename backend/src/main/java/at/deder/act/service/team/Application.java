package at.deder.act.service.team;

import at.deder.act.service.team.inbound.rest.RestInterface;
import at.deder.act.service.team.outbound.audit.AuditLogService;
import com.google.inject.Guice;

public class Application {
    public static void main(String... args) {
        var injector = Guice.createInjector(new ApplicationModule());

        var auditService = injector.getInstance(AuditLogService.class);
        var inboundRest = new RestInterface(injector);

        inboundRest.start();
        auditService.start();
    }
}
