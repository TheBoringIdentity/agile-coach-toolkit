package at.deder.act.service.team.domain.core.team;

import at.deder.act.service.team.domain.framework.DomainEvent;

public class TeamRenamedEvent implements DomainEvent {
    final private String teamId;
    final private TeamName oldName;
    final private TeamName newName;

    public TeamRenamedEvent(String teamId, TeamName oldName, TeamName newName) {

        this.teamId = teamId;
        this.oldName = oldName;
        this.newName = newName;
    }

    public TeamName getOldName() {
        return oldName;
    }

    public TeamName getNewName() {
        return newName;
    }

    public String getTeamId() {
        return teamId;
    }
}
