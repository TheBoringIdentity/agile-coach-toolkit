package at.deder.act.service.team.domain.core.team;

import java.time.LocalDateTime;
import java.util.Objects;

public class Note {
    private LocalDateTime date;
    private String content;

    public Note(LocalDateTime date, String content) {
        this.date = date;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return Objects.equals(date, note.date) &&
                Objects.equals(content, note.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, content);
    }

    @Override
    public String toString() {
        return "Note{" +
                "date=" + date +
                ", content='" + content + '\'' +
                '}';
    }
}
