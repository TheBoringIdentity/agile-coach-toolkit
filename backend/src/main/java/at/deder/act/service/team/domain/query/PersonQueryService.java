package at.deder.act.service.team.domain.query;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import com.google.inject.Inject;

import java.util.Collection;
import java.util.stream.Collectors;

public class PersonQueryService {
    private PersonRepository repository;

    @Inject
    public PersonQueryService(PersonRepository repository) {
        this.repository = repository;
    }

    public Collection<Person> queryAllPeople() {
        var ids = repository.queryAllPersonIds();
        var persons = ids.stream().map(id -> repository.getPersonById(id)).collect(Collectors.toSet());
        return persons;
    }
}
