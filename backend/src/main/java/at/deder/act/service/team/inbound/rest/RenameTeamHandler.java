package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.command.RenameTeam;
import at.deder.act.service.team.domain.command.RenamedTeamDoesNotExist;
import at.deder.act.service.team.domain.core.people.PersonName;
import at.deder.act.service.team.domain.core.team.TeamName;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

class RenameTeamHandler {

    private final TeamRepository repository;
    private DomainEventDispatcher dispatcher;

    @Inject
    RenameTeamHandler(TeamRepository repository, DomainEventDispatcher dispatcher) {
        this.repository = repository;
        this.dispatcher = dispatcher;
    }

    Response handleTeamRenameRequest(Request request, Response response) {
        RenameTeamRequestData requestData = RenameTeamRequestData.parse(request, response);
        if(!requestData.isValid()) {
            return response;
        }

        try {
            renameTeam(requestData);
        } catch (RenamedTeamDoesNotExist e) {
            response.status(HttpStatus.NOT_FOUND_404);
        }

        response.status(HttpStatus.OK_200);
        return response;
    }

    private void renameTeam(RenameTeamRequestData requestData) throws RenamedTeamDoesNotExist {
        var command = new RenameTeam(repository, dispatcher);

        var name = requestData.getName();
        var id = requestData.getId();

        command.renameTeam(id, new TeamName(name));
    }
}