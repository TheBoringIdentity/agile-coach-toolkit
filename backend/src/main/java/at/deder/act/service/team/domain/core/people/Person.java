package at.deder.act.service.team.domain.core.people;

import org.apache.commons.lang3.Validate;

public class Person {
    private PersonName name;
    private String id;
    private boolean active;

    public void setName(PersonName name) {
        Validate.notNull(name);
        this.name = name;
    }

    public PersonName getName() {
        return name;
    }

    public void setId(String id) {
        Validate.notNull(id);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setActive(boolean activeStatus) {
        this.active = activeStatus;
    }

    public boolean isActive() {
        return active;
    }
}
