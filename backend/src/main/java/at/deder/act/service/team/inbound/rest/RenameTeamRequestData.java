package at.deder.act.service.team.inbound.rest;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;

class RenameTeamRequestData extends AbstractRequestData {
    private String name;
    private String id;

    private RenameTeamRequestData() {
    }

    String getName() {
        return name;
    }

    String getId() {
        return id;
    }

    private void setData(JsonElement nameJson, String id) {
        this.id = id;
        this.name = nameJson.getAsString();

    }

    static RenameTeamRequestData parse(Request request, Response response) {
        var requestData = new RenameTeamRequestData();

        JsonObject root = convertFromJson(request);
        if (root == null) {
            requestData.invalidate(response);
            return requestData;
        }

        var nameJson = root.get("name");
        if (nameJson == null) {
            requestData.invalidate(response);
            return requestData;
        }

        var id = request.params("id");
        if (id == null) {
            requestData.invalidate(response);
            return requestData;
        }

        requestData.setData(nameJson, id);
        requestData.setValid();
        return requestData;
    }
}