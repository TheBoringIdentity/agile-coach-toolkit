package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.query.TeamQueryService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import java.util.Map;

public class GetTeamsHandler {
    private final TeamQueryService queryService;

    @Inject
    public GetTeamsHandler(TeamQueryService queryService) {
        this.queryService = queryService;
    }

    public String handleRequest(Request mockRequest, Response mockResponse) {
        Map<String, String> idsAndNames = queryService.queryTeamIdNameMapping();
        mockResponse.status(HttpStatus.OK_200);
        return new Gson().toJson(idsAndNames);
    }
}
