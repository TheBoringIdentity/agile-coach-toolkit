
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'teams', component: () => import('pages/Teams.vue') },
      { path: 'team/:id', component: () => import('pages/TeamDetail.vue') },
      { path: 'people', component: () => import('pages/People.vue') },
      { path: 'action/addNote', component: () => import('pages/AddNote.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
