import config from '../config.js'
import axios from 'axios'

class APIError {
  constructor (message) {
    this.message = message
    this.name = 'APIError'
  }
}

class TeamAPI {
  constructor (url) {
    this._url = url
  }

  fetchTeamName (teamId) {
    console.log(config.API_URL)
    return axios.get(config.API_URL + '/team/' + teamId + '/name').then(response => {
      return response.data
    })
  }

  getIdsAndNames () {
    return axios.get(config.API_URL + '/team').then(response => {
      return response.data
    })
  }

  addTeam (name) {
    return axios.put(config.API_URL + '/team', { name: name })
  }

  getNameOf (id) {
    return axios.get(config.API_URL + '/team/' + id + '/name').then(response => {
      if (response.status !== 200) {
        throw new APIError('request failed: ' + response.statusText + ' - ' + response.status)
      }
      return response.data
    })
  }

  getNotesOf (id) {
    return axios.get(config.API_URL + '/team/' + id + '/notes').then(response => { return response.data })
  }

  addNote (id, note) {
    axios.patch(config.API_URL + '/team/' + id + '/notes', { content: [note] }).then(response => {
      if (response.status !== 200) {
        throw new APIError('note post failed: ' + response.statusText + ' - ' + response.status)
      }
    }).catch(e => { throw new APIError(e.toString()) })
  }

  exists (id) {
    return this.getIdsAndNames().then(mapping => mapping[id] !== undefined).catch(e => { throw new APIError(e) })
  }
}

class PeopleAPI {
  constructor (url) {
    this._url = url
  }

  async getAllPeople () {
    return axios.get(this._url + '/person').then(response => {
      return response.data
    })
  }

  async addPerson (name) {
    return axios.put(this._url + '/person', {
      name: name
    }).catch(e => { throw new APIError(e) })
  }
}

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Vue.prototype.$api = {
    team: new TeamAPI(config.API_URL),
    people: new PeopleAPI(config.API_URL)
  }
}
